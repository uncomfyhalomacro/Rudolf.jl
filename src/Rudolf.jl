# SPDX-License-Identifier: MPL-2.0

"""
# Rudolf

Rudolf.jl is a pure Julia library for working with [RDF](https://www.w3.org/RDF/). Rudolf.jl contains most things you need to work with RDF, including:

- parsers and serializers for RDF/XML, N3, NTriples, N-Quads, Turtle, TriX, Trig and JSON-LD
- a Graph interface which can be backed by any one of a number of Store implementations
- store implementations for in-memory, persistent on disk (Berkeley DB) and remote SPARQL endpoints
- a SPARQL 1.1 implementation - supporting SPARQL 1.1 Queries and Update statements
- SPARQL function extension mechanisms

## License

Copyright (C) 2023  Soc Virnyl Estela
 
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
"""
module Rudolf

# Write your package code here

export read
export write
export Turtle
export SPARQL
export RDFXML
export NThree
export NTriples
export NQuads
export Trix
export Trig
export JSONLD
export RDFSchema
export RDF_MIMETYPE

"""
    RDF_MIMETYPE

Internet media type / MIME Type for RDF/XML.
"""
const RDF_MIMETYPE = raw"application/rdf+xml"

include("./RDFSchema.jl")
include("./NThree.jl")
include("./NTriples.jl")
include("./NQuads.jl")
include("./Trix.jl")
include("./Trig.jl")
include("./JSONLD.jl")
include("./RDFXML.jl")
include("./Turtle.jl")
include("./SPARQL.jl")

end
