#= 
SPDX-License-Identifier: MPL-2.0
Copyright (C) 2023  Soc Virnyl Estela

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
=#

"""
    RDFSchema

This module provides the RDF Schema vocabulary in the forms of types or methods.

RDF Schema provides a data-modelling vocabulary for RDF data. RDF Schema is an extension of the basic RDF vocabulary.

More information about it can be found at https://www.w3.org/TR/rdf-schema/#abstract.
"""
module RDFSchema

# TODO More code here
"""
    AbstractRDFResource

All things described by RDF are called resources.
"""
abstract type AbstractRDFResource end

"""
    AbstractRDFClass <: AbstractRDFResource

All instances of all classes are also themselves resources.
Classes are defined by the abstract type `AbstractRDFClass`.

`AbstractRDFClass` is a subtype `AbstractRDFResource`.
"""
abstract type AbstractRDFClass <: AbstractRDFResource end

"""
    AbstractRDFLiteral <: AbstractRDFResource

This abstract type represents the class `rdfs:Literal`.

It represents the class of literal values such as strings
and integers.
"""
abstract type AbstractRDFLiteral <: AbstractRDFResource end

"""
    AbstractRDFDatatype <: AbstractRDFLiteral

This abstract type is the supertype of all RDF model of
datatype. It is a subtype of `AbstractRDFLiteral` which
represents literal values.
"""
abstract type AbstractRDFDatatype <: AbstractRDFLiteral end

"""
    AbstractRDFlangString <: AbstractRDFDatatype

Abstract type for language-tagged string values.
"""
abstract type AbstractRDFlangString <: AbstractRDFDatatype end

"""
    AbstractRDFHTML <: AbstractRDFDatatype

Abstract type for HTML literal values.
"""
abstract type AbstractRDFHTML <: AbstractRDFDatatype end

"""
    AbstractRDFXMLLiteral <: AbstractRDFDatatype

Abstract type for XML literal values.
"""
abstract type AbstractRDFXMLLiteral <: AbstractRDFDatatype end

"""
    AbstractRDFJSON <: AbstractRDFDatatype

Abstract type for JSON literal values.
"""
abstract type AbstractRDFJSON <: AbstractRDFDatatype end

abstract type AbstractRDFProperty <: AbstractRDFClass end

# struct RDFClass <: AbstractRDFClass
# end

# struct RDFResource <: AbstractRDFResource
# end

# struct RDFProperty <: AbstractRDFProperty
# end

# struct RDFrange <: AbstractRDFProperty

# end

# struct RDFdomain <: AbstractRDFProperty
# end

# struct RDFtype <: AbstractRDFProperty
# end

# TODO should this be a method?
# struct RDFsubClassOf <: AbstractRDFProperty
# end

# TODO should this be a method too?
# struct RDFsubPropertyOf <: AbstractRDFProperty
# end

abstract type AbstractRDFTriple end

struct RDFlangString <: AbstractRDFlangString
    string_literal::String
end

"""
    RDFLiteral

This type represent literal values as defined in the RDF Schema Document.

Literals are used for values such as strings, numbers, and dates.

A ***literal*** in an RDF graph consists of two, three, or four elements:
- a lexical form
- a datatype IRI
- a language tag
- a base direction for the third element. Either `:ltr` or `:rtl`

More information about this can be read at https://www.w3.org/TR/rdf12-concepts/#section-Graph-Literal

"""
struct RDFLiteral{L <: AbstractRDFLiteral, D <: AbstractRDFDatatype}
    value::L
    datatype::D
    language_tag::RDFlangString
    base_direction::Symbol
end

"""
    Triple

An ***RDF graph*** is a set of RDF triples. 

See https://www.w3.org/TR/rdf12-concepts/#dfn-rdf-triple for more information.
"""
struct Triple{C <: AbstractRDFClass, P <: AbstractRDFProperty} <: AbstractRDFTriple
    first::C
    second::P
    third::RDFLiteral
end

end
