# Rudolf

Rudolf.jl is a pure Julia library for working with [RDF](https://www.w3.org/RDF/). Rudolf.jl contains most things you need to work with RDF, including:

- parsers and serializers for RDF/XML, N3, NTriples, N-Quads, Turtle, TriX, Trig and JSON-LD
- a Graph interface which can be backed by any one of a number of Store implementations
- store implementations for in-memory, persistent on disk (Berkeley DB) and remote SPARQL endpoints
- a SPARQL 1.1 implementation - supporting SPARQL 1.1 Queries and Update statements
- SPARQL function extension mechanisms

[![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://uncomfyhalomacro.codeberg.page/Rudolf/stable/)
[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://uncomfyhalomacro.codeberg.page/Rudolf/dev/)
[![Code Style: SciML](https://img.shields.io/static/v1?label=code%20style&message=SciML&color=9558b2&labelColor=389826)](https://github.com/SciML/SciMLStyle)
[![CI Status](https://ci.codeberg.org/api/badges/uncomfyhalomacro/Rudolf.jl/status.svg)](https://ci.codeberg.org/uncomfyhalomacro/Rudolf)

