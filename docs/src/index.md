```@meta
CurrentModule = Rudolf
```

# Rudolf

Documentation for [Rudolf](https://codeberg.org/uncomfyhalomacro/Rudolf.jl).

```@index
```

```@autodocs
Modules = [
  Rudolf
]
```
