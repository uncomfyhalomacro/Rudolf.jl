cd(@__DIR__)
using Pkg;
Pkg.activate(@__DIR__);
Pkg.instantiate();
push!(LOAD_PATH, "../")
using Rudolf
using Documenter

@info "Setting up documentation for Rudolf"

DocMeta.setdocmeta!(Rudolf, :DocTestSetup, :(using Rudolf); recursive = true)

custom_exceptions = Documenter.except()

# TODO Open a PR to change `CI_BUILD_EVENT` to `CI_PIPELINE_EVENT`.
ENV["CI_BUILD_EVENT"] = try
    ENV["CI_PIPELINE_EVENT"]
catch e
    @error "Got error $e"
    error("$e")
end

makedocs(;
    sitename = "Rudolf",
    modules = [Rudolf],
    authors = "Soc Virnyl S. Estela <contact@uncomfyhalomacro.pl> and contributors",
    repo = "https://codeberg.org/uncomfyhalomacro/Rudolf.jl/_edit/{commit}/{path}",
    format = Documenter.HTML(;
        prettyurls=  get(ENV, "CI", "false") in ["woodpecker"],
        canonical = "https://uncomfyhalomacro.codeberg.page/Rudolf.jl",
        repolink = "https://codeberg.org/uncomfyhalomacro/Rudolf.jl",
        assets = String[]),
    pages = [
        "Home" => "index.md",
    ],
    warnonly = custom_exceptions,
    clean = true,
    doctest = true)

deploydocs(;
    repo = "codeberg.org/uncomfyhalomacro/Rudolf.jl.git",
    devbranch = "main",
    target = "build",
    branch = "pages",
    push_preview = true,
    forcepush = true)
