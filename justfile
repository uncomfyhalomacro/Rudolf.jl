default: all-deps test


all-deps: get-julia doc-deps
		/usr/bin/julia --project=. --startup-file=no --history-file=no -e "import Pkg; Pkg.instantiate()"

get-julia:
		/usr/bin/zypper --non-interactive install juliaup openssh git
		/usr/bin/juliaup add release
		/usr/bin/juliaup default release

doc-deps: 
		/usr/bin/julia --project=docs/ --startup-file=no --history-file=no -e "import Pkg; Pkg.instantiate()"


push-docs:
		/usr/bin/julia --project=docs/ --startup-file=no --history-file=no docs/make.jl


# Rudolf tests. Integrate this with Aqua.jl in the future
test:
		/usr/bin/julia --project=. --startup-file=no --history-file=no -e "import Pkg; Pkg.test()"


		
